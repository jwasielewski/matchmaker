package pl.jwasielewski.matchmaker.boundary;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MatchMaker {
    public static final Logger logger = LoggerFactory.getLogger(MatchMaker.class);

    public static void main(String[] args) {
        new MatchMaker().run();
    }

    public MatchMaker() {
    }

    public void run() {
        logger.info("Starting MatchMaker");
    }
}
