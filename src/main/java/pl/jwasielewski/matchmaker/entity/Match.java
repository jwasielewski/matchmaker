package pl.jwasielewski.matchmaker.entity;

import java.util.Objects;

public class Match {
    private Player player1;
    private Player player2;
    private boolean player1PlayingAtHome;

    public Match(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer1PlayingAtHome(boolean player1PlayingAtHome) {
        this.player1PlayingAtHome = player1PlayingAtHome;
    }

    public boolean isPlayer1PlayingAtHome() {
        return player1PlayingAtHome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Match match = (Match) o;
        return (Objects.equals(player1, match.player1) &&
                Objects.equals(player2, match.player2))
                ||
                (Objects.equals(player1, match.player2) &&
                        Objects.equals(player2, match.player1));
    }

    @Override
    public int hashCode() {
        return Objects.hash(player1, player2);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(player1.getName()).append(player1PlayingAtHome ? " (H)" : "    ");
        sb.append(" - ");
        sb.append(player1PlayingAtHome ? "    " : "(H) ").append(player2.getName());
        return sb.toString();
    }

}
