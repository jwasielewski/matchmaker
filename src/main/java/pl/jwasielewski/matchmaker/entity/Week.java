package pl.jwasielewski.matchmaker.entity;

import java.util.*;

public class Week {
    private List<Match> matches;

    public Week() {
        matches = new ArrayList<>();
    }

    public void add(Match match) {
        matches.add(match);
    }

    public boolean containsPlayers(Match match) {
        Set<String> players = new HashSet<>();
        players.add(match.getPlayer1().getName());
        players.add(match.getPlayer2().getName());
        matches.forEach(m -> {
            players.add(m.getPlayer1().getName());
            players.add(m.getPlayer2().getName());
        });

        return players.size() < (matches.size() * 2 + 2);
    }

    public int size() {
        return matches.size();
    }

    public Match get(int i) {
        return matches.get(i);
    }

    public void shuffle() {
        Collections.shuffle(matches);
    }
}
