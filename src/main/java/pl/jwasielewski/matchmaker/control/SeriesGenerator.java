package pl.jwasielewski.matchmaker.control;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.jwasielewski.matchmaker.entity.Match;
import pl.jwasielewski.matchmaker.entity.Week;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SeriesGenerator {
    private static final Logger logger = LoggerFactory.getLogger(SeriesGenerator.class);

    public List<Week> generate(List<Match> matches) {
        List<Week> series = new LinkedList<>();
//        Collections.shuffle(matches);

        for (Match playerMatch : matches) {
            boolean hasBeenAdded = false;

            for (Week week : series) {
                if (!week.containsPlayers(playerMatch)) {
                    week.add(playerMatch);
                    hasBeenAdded = true;
                    break;
                }
            }

            if (!hasBeenAdded) {
                Week w = new Week();
                w.add(playerMatch);
                series.add(w);
            }
        }

//        series.forEach(Week::shuffle);
//        Collections.shuffle(series);

        return series;
    }

}
