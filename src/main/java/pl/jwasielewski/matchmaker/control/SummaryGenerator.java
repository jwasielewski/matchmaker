package pl.jwasielewski.matchmaker.control;

import pl.jwasielewski.matchmaker.entity.Match;
import pl.jwasielewski.matchmaker.entity.Player;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SummaryGenerator {

    public String generate(List<Match> matches) {
        Map<Player, Integer> cache = new HashMap<>();
        for (Match match : matches) {
            cache.putIfAbsent(match.getPlayer1(), 0);
            cache.putIfAbsent(match.getPlayer2(), 0);

            if (match.isPlayer1PlayingAtHome()) {
                cache.put(match.getPlayer1(), cache.get(match.getPlayer1()) + 1);
                continue;
            }

            cache.put(match.getPlayer2(), cache.get(match.getPlayer2()) + 1);
        }

        return cache.entrySet()
                .stream()
                .sorted(Comparator.comparing(a -> a.getKey().getName()))
                .map(e -> String.format("%s - %d", e.getKey().getName(), e.getValue()))
                .collect(Collectors.joining("\n"));
    }

}
