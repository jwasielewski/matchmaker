package pl.jwasielewski.matchmaker.control;

import pl.jwasielewski.matchmaker.entity.Match;
import pl.jwasielewski.matchmaker.entity.Player;

import java.util.*;

public class MatchCreator {

    public List<Match> create(List<Player> players) {
        Set<Match> matchesCache = new LinkedHashSet<>();

        for (int i = 0; i < players.size(); i++) {
            Player a = players.get(i);

            for (int j = i + 1; j < players.size(); j++) {
                if (a.equals(players.get(j))) {
                    continue;
                }

                matchesCache.add(new Match(a, players.get(j)));
            }
        }

        List<Match> matches = new ArrayList<>(matchesCache);
        decideWhereMatchIsPlaying(matches, players.size());

        return matches;
    }

    private void decideWhereMatchIsPlaying(List<Match> matches, int numberOfPlayers) {
        Map<Player, Integer> homeMatechesLog = new HashMap<>();

        for (Match match : matches) {
            homeMatechesLog.putIfAbsent(match.getPlayer1(), 0);
            homeMatechesLog.putIfAbsent(match.getPlayer2(), 0);

            if (homeMatechesLog.get(match.getPlayer1()) < howManyMatchesShouldBePlayedAtHome(numberOfPlayers)) {
                match.setPlayer1PlayingAtHome(true);
                homeMatechesLog.put(match.getPlayer1(), homeMatechesLog.get(match.getPlayer1()) + 1);
                continue;
            }

            match.setPlayer1PlayingAtHome(false);
            homeMatechesLog.put(match.getPlayer2(), homeMatechesLog.get(match.getPlayer2()) + 1);
        }
    }

    private int howManyMatchesShouldBePlayedAtHome(int numberOfPlayers) {
        return Math.round((numberOfPlayers - 1) / 2.0f);
    }

}
