package pl.jwasielewski.matchmaker.control;

import org.junit.Before;
import org.junit.Test;
import pl.jwasielewski.matchmaker.entity.Match;
import pl.jwasielewski.matchmaker.entity.Player;
import pl.jwasielewski.matchmaker.entity.Week;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MatchCreatorTest {

    private MatchCreator sut;

    @Before
    public void setUp() {
        sut = new MatchCreator();
    }

    @Test
    public void create1() {
        List<Player> players = Arrays.asList(
                new Player("A"),
                new Player("B"),
                new Player("C"),
                new Player("D"),
                new Player("E"),
                new Player("F"),
                new Player("G"),
                new Player("H"),
                new Player("I"),
                new Player("J"),
                new Player("K"),
                new Player("L"),
                new Player("M"),
                new Player("N"),
                new Player("O"),
                new Player("P"),
                new Player("Q"),
                new Player("R"),
                new Player("S"),
                new Player("T"),
                new Player("U"),
                new Player("V"),
                new Player("W"),
                new Player("X"),
                new Player("Y"),
                new Player("Z")
        );

        List<Match> matches = sut.create(players);
        matches.forEach(System.out::println);
        System.out.println("\n================\n");
        System.out.println(new SummaryGenerator().generate(matches));
        System.out.println("\n================\n");
        List<Week> series = new SeriesGenerator().generate(matches);
        for (int i = 0; i < series.size(); i++) {
            System.out.println("Series " + (i + 1));
            for (int j = 0; j < series.get(i).size(); j++) {
                System.out.println(series.get(i).get(j));
            }
            System.out.println();
        }
    }

    @Test
    public void create2() {
        List<Player> players = Arrays.asList(
                new Player("A"),
                new Player("B"),
                new Player("C"),
                new Player("D"),
                new Player("E"),
                new Player("F"),
                new Player("G")
        );

        List<Match> matches = sut.create(players);
        matches.forEach(System.out::println);
        System.out.println("\n================\n");
        System.out.println(new SummaryGenerator().generate(matches));
        System.out.println("\n================\n");
        List<Week> series = new SeriesGenerator().generate(matches);
        for (int i = 0; i < series.size(); i++) {
            System.out.println("Series " + (i + 1));
            for (int j = 0; j < series.get(i).size(); j++) {
                System.out.println(series.get(i).get(j));
            }
            System.out.println();
        }
    }

}